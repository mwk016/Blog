---
title: Design Challenge blog
subtitle: 8 september
date: 2017-09-08
tags: ["blog",]
---

Dag 2 van de design challenge is voorbij en we hebben een beetje vooruitgang geboekt.

#### Moodboard
In de ochtend zijn we begonnen met het maken van een moodboard. Dit was bedoeld om een visie te krijgen van ons doelgroep (CMD 1e-jaarsstudenten) en iedereen moest een eigen moodboard maken. We hebben het lekker 'klassiek' gehouden, dus op een a3'tje plaatjes van wat tijdschriften plakken. Hieronder mijn moodboard: 

![enter image description here](https://lh3.googleusercontent.com/8JdecAxUw6HDmwHGp0aIhR_afD70n2S-UTocuLX6j-_PMeRPlN9nWUxq99uwsHAvji846frHMyg=s0 "20170908_115011.jpg")

Ik ben er zelf nog niet tevreden mee, omdat hij nog best kaal is. Bij het 
inlevermoment hoop ik hierop feedback te krijgen.

#### Workshops
Nadat we klaar waren met de moodboards en de grote pauze kregen we een aantal workshops over het maken van een moodboard en het maken van de blog. Het moodboard workshop kwam iets te laat voor ons, omdat we het al af hadden, maar de blog workshop waas zeker nuttig, al had ik graag willen horen hoe je een goede blog kan schrijven.

#### Planning maken en mijn planning voor dinsdag

Na de workshops ben ik Vanity gaan helpen met het maken van de planning. Zij zette en structureerde de planning op excel en ik zei wat er gedaan moet worden en wanneer het gedaan moet worden.

![enter image description here](https://lh3.googleusercontent.com/-fuZDTz0pA3I/WbpoQ0DTS7I/AAAAAAAAAFM/hcOYS6kazMkxVqA0HZjHwB2KcXpN7IH6ACLcBGAs/s0/Screen+Shot+09-14-17+at+01.29+PM.PNG "Screen Shot 09-14-17 at 01.29 PM.PNG")

Zoals op de planning hierboven is te zien wil ik dinsdag helpen met de enquête. Als de resultaten hiervan binnen zijn kunnen we een visuele onderzoek maken van de resultaten van de enquête. Daarna probeer ik een beginnetje te maken aan de paper prototype, maar voordat ik echt begin hieraan wil ik eerst een beetje schetsen waar alles moet staan, hoe het eruit moet zien enz., zodat we zonder moeite de uiteindelijke paper prototype kunnen maken.



