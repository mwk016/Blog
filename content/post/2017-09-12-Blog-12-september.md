---
title: Design Challenge blog
subtitle: 12 september
date: 2017-09-12
tags: ["blog",]
---
De tweede week van de Design Challenge is begonnen. Het was een drukke (en vooral een chaotische dag).

Chris en ik zijn de dag begonnen met het definitieve concept op papier te zetten. Bijna alles is hetzelfde gebleven (zie blog 5 september). De enige veranderingen die we hebben gemaakt is dat de tour gebaseerd is op punten in plaats van tijd en dat je bij elke opdracht een kleine beloning krijgt. Verder willen we een GPS tracker toevoegen. Je maakt een foto van jezelf of je team, om hem daarna weer terug te vinden op de kaart. Daarnaast kan je ook je tegenstanders zien, om in de gaten te houden hoe ver zij zijn.
![enter image description here](https://lh3.googleusercontent.com/IzoBMEvqVBzu1vB8RS_Q_b2_5l_N5CaxUUqgHAB9TV_fdKldQvTcdCK0Xeck9SfL4FyGd4Pu-Yk=s0 "IMG-20170912-WA0002[1].jpg")

Na de pauze kregen we een nieuwe teamgenoot bij, Dilara. We hebben kennis gemaakt en gepraat over haar kwaliteiten. Uit de Belbin test is uitgekomen dat ze een 'plant' is. We zijn er ook achter gekomen dat ze heel goed kan tekenen, precies wat we nodig hadden voor de paper prototype.

Naast de uitslagen berekenen van ons enquête hebben we verder niet veel meer gedaan aan het project, omdat we een aantal workshops kregen (website blog en coaching). Door de slechte planning hiervan eindigde onze dag chaotisch, omdat we door die workshops weinig tijd krijgen om te werken aan het project en dat de timing van de workshops (bijvoorbeeld op vrijdag 15:00 info over prototyping, terwijl we om 17:00 ons werk in moeten leveren). 

![enter image description here](https://lh3.googleusercontent.com/-IKTPDFA-PsY/Wb_rQh-J-UI/AAAAAAAAAGE/-z9w07HhJuEt57I-a78JpbB3o-1RtG0bwCLcBGAs/s0/IMG-20170912-WA0000%255B1%255D.jpg "IMG-20170912-WA0000[1].jpg")

Als ik thuis ben, ga ik me voorbereiden op vrijdag, dus probeer ik te oriënteren op de field research en ga ik schetsen maken voor de paper prototype. Voor vrijdag hoeven we dan niet veel moeite meer te doen voor de paper prototype en kunnen we rustig werken aan de visuals van de onderzoek en de field research.

