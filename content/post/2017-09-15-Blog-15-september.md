Dag twee van de tweede week van de Design Challenge is begonnen. Vandaag is de deadline van de eerste iteratie en moeten we onze deliverables inleveren. Althans, dat was de bedoeling. In verband met tijdtekort is de deadline nu verschoven naar aanstaande maandag

### Paper prototype en visueel onderzoek
Voordat de Design Challenge les begon ben ik op donderdag een schets gaan maken hoe de paper prototype eruit moet zien.

![enter image description here](https://lh3.googleusercontent.com/-NdvdJ-Z5J0s/WclU7971AaI/AAAAAAAAAHM/vqmvtzxeI_IRxP_o31DxPnU2YUNlbcT6gCLcBGAs/s0/20170915_105453%255B1%255D.jpg "20170915_105453[1].jpg")

Ik heb de schets aan de rest van het groepje laten zien. We zijn gelijk aan de slag gegaan en we hebben doorgewerkt tot aan de grote pauze. Ik was bezig geweest ,et de setup van de app.

Na de pauze hebben we de paper prototype eventjes links gelegd, omdat we wat materialen moesten kopen zoals karton, kleur, lijm, plakband enz. Dus zijn we verder gegaan met het maken van de grafieken, gebaseerd op de uitslagen van de enquete.

![enter image description here](https://lh3.googleusercontent.com/-xXyvpR5WIsQ/WclSv5HBhcI/AAAAAAAAAG8/UrblA_gmCAYfOSAyV-ahrE1RNAThfYTHwCLcBGAs/s0/Screen+Shot+09-17-17+at+05.09+PM+001.PNG "Screen Shot 09-17-17 at 05.09 PM 001.PNG")



Nadat we klaar waren met de grafieken zijn we even naar de Action gegaan om wat materialen te halen voor de paper prototype. Toen we terug waren ben ik gelijk aan de slag gegaan met het maken van een kartonnen smartphone.
![enter image description here](https://lh3.googleusercontent.com/-gXG34H_Gbsw/WclVcFQhb1I/AAAAAAAAAHc/7g3l759o1bQALT1rW7fgq7L6_oUVoCa3gCLcBGAs/s0/20170915_162126%255B1%255D.jpg "20170915_162126[1].jpg")
![enter image description here](https://lh3.googleusercontent.com/-SCUpTHDabyI/WclVMNpwPqI/AAAAAAAAAHU/Bff--TJVKUUQyaxQ5Zq7ezp0WpD95q7-ACLcBGAs/s0/20170915_162421%255B1%255D.jpg "20170915_162421[1].jpg")

Voor maandag kunnen we nog eventuele dingen toevoegen aan de prototype en hopen we nog de paper prototype te kunnen testen. Dinsdag is dan de presentatie.

